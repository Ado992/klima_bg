CREATE MATERIALIZED VIEW air_conditions_monthly_mview
      REFRESH NEXT TRUNC(SYSDATE+1/24,'HH')	AS
select time, round(avg(lufttemperatur),1)as MEAN,max(lufttemperatur) as MAXTEMP,min(lufttemperatur)AS MINTEMP from (
SELECT TIME,TO_CHAR(round(row_number() over(order by TIME))) RECORD_ID,TIME_SENSOR,LUFTTEMPERATUR
 from (  SELECT * FROM (
    -- Station Blaueis --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl,1) LuftTemperatur
		FROM klima.blaueis
        
 UNION
         -- Station Brunftbergtiefe --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl,1) LuftTemperatur
		FROM klima.brunftbergtiefe 
        
UNION
         -- Station Funtseetauern --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl_ftauern,1) LuftTemperatur
		FROM klima.kuehroint_lwd 
        
UNION
         -- Station Hinterberghorn --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl,1) LuftTemperatur
		FROM klima.hinterberghorn 
        
UNION
         -- Station Hinterseeau --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl,1) LuftTemperatur
		FROM klima.hinterseeau 
                   
UNION
         -- Station Höllgraben --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl,1) LuftTemperatur
		FROM klima.hoellgraben_lwd 
      
UNION
         -- Station Jenner --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl,1) LuftTemperatur
		FROM klima.jenner_lwd 
       
UNION
         -- Station Kühroint --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl_kueh,1) LuftTemperatur
		FROM klima.kuehroint_lwd 
                  
UNION
         -- Station Reiteralm --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl1670,1) LuftTemperatur
		FROM klima.reiteralpe_lwd 
        
UNION
         -- Station Schlunghorn --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl,1) LuftTemperatur
		FROM klima.schlunghorn 
       
UNION
         -- Station Schoenau --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl,1) LuftTemperatur
		 FROM klima.schoenau 
                
UNION
         -- Station Steinernes Meer  --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl1893_6,1) LuftTemperatur
		FROM klima.steinernes_meer 
        
UNION
         -- Station Trischübel --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl,1) LuftTemperatur
		FROM klima.trischuebel 
         
UNION
         -- Station Watzmanngrat --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl,1) LuftTemperatur
		FROM klima.watzmanngrat 
       
UNION
         -- Station Watzmannhaus --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(tl,1) LuftTemperatur
		 FROM klima.watzmannhaus 
        )))    
group by time

