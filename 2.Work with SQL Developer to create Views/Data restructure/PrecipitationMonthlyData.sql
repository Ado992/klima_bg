CREATE MATERIALIZED VIEW prec_conditions_monthly_mview
      REFRESH NEXT TRUNC(SYSDATE+1/24,'HH')	AS
      select time, sum(niederschlag) as Niederschlag, round(avg(schneehohe),1) as Schneehohe from (
SELECT TIME,TIME_SENSOR,NIEDERSCHLAG,SCHNEEHOHE
 from (  SELECT * FROM (
    -- Station Blaueis --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(null,0)Niederschlag,
        round(sh,0)Schneehohe
        FROM klima.blaueis 
UNION
         -- Station Brunftbergtiefe --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(n,0)Niederschlag,
        round(sh,0)Schneehohe
        FROM klima.brunftbergtiefe 
      
UNION
         -- Station Funtseetauern --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(null,0)Niederschlag,
        round(null,0)Schneehohe
       FROM klima.kuehroint_lwd 
        
UNION
         -- Station Hinterberghorn --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(null,0)Niederschlag,
        round(null,0)Schneehohe
        FROM klima.hinterberghorn 
      
UNION
         -- Station Hinterseeau --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(n,0)Niederschlag,
        round(sh,0)Schneehohe
        FROM klima.hinterseeau 
                   
UNION
         -- Station Höllgraben --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(n,0)Niederschlag,
        round(null,0)Schneehohe
        FROM klima.hoellgraben_lwd 
        
UNION
         -- Station Jenner --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(null,0)Niederschlag,
        round(sh,0)Schneehohe
        FROM klima.jenner_lwd 
       
UNION
         -- Station Kühroint --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(n,0)Niederschlag,
        round(sh,0)Schneehohe
        FROM klima.kuehroint_lwd 
                    
UNION
         -- Station Reiteralm --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(n,0)Niederschlag,
        round(sh1670,0)Schneehohe
        FROM klima.reiteralpe_lwd 
        
UNION
         -- Station Schlunghorn --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(null,0)Niederschlag,
        round(sh,0)Schneehohe
        FROM klima.schlunghorn 
       
UNION
         -- Station Schoenau --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(n,0)Niederschlag,
        round(null,0)Schneehohe
       FROM klima.schoenau 
                 
UNION
         -- Station Steinernes Meer  --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(n,0)Niederschlag,
        round(sh,0)Schneehohe
        FROM klima.steinernes_meer 
        
UNION
         -- Station Trischübel --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(n,0)Niederschlag,
        round(sh,0)Schneehohe
        FROM klima.trischuebel 
         
UNION
         -- Station Watzmanngrat --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(null,0)Niederschlag,
        round(null,0)Schneehohe
       FROM klima.watzmanngrat 
        
UNION
         -- Station Watzmannhaus --
	  SELECT TO_CHAR(datum, 'rrrr-mm') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm')),0) ID,
		TO_CHAR(datum, 'rrrr-mm') Time_Sensor,
		round(null,0)Niederschlag,
        round(null,0)Schneehohe
        FROM klima.watzmannhaus )))
group by time
       
                
