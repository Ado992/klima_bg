CREATE MATERIALIZED VIEW sun_hourly
      REFRESH NEXT TRUNC(SYSDATE+1/24,'HH')	AS
SELECT ALTITUDE, TIME,round(row_number() over(order by ALTITUDE)) RECORD_ID,E_YEAR,SONNENSCHEINDAUER,GLOBALSTRAHLUNG,REFLEKTIERTESSTRAHLUNG,DIREKTESTSTRAHLUNG,LUFTTEMPERATUR,RELATIVELUFTFEUCHTIGKEIT,STATION,LONGITUDE,LATITUDE,SDO_GEOMETRY(2001,8307,SDO_POINT_TYPE(LONGITUDE, LATITUDE, NULL),NULL,NULL) Shape
 from (  SELECT * FROM (
    -- Station Blaueis --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(cast(null as INT),0)Sonnenscheindauer,
        round(ge,1)GlobalStrahlung,
        round(cast(null as INT),1)ReflektiertesStrahlung,
        round(cast(null as INT),1)DirektestStrahlung,
        round(tl,1) LuftTemperatur,
        round(f,0) RelativeLuftfeuchtigkeit,
		'Blaueis' Station,
		to_number('12,870189') Longitude,
		to_number('47,586678') Latitude,
		to_number('1650') Altitude
	  FROM klima.blaueis 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    SUM(ge) ge,
                    AVG(tl)tl,
                    AVG(f) f
                    FROM klima.blaueis 
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2018)
UNION
         -- Station Brunftbergtiefe --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(cast(null as INT),0)Sonnenscheindauer,
        round(ge,1)GlobalStrahlung,
        round(gr,1)ReflektiertesStrahlung,
        round(cast(null as INT),1)DirektestStrahlung,
        round(tl,1) LuftTemperatur,
        round(f,0) RelativeLuftfeuchtigkeit,
		'Brunftbergtiefe' Station,
		to_number('12,877646') Longitude,
		to_number('47,548542') Latitude,
		to_number('1240') Altitude
	  FROM klima.brunftbergtiefe 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    SUM(ge) ge,
                    SUM(gr)gr,
                    AVG(tl)tl,
                    AVG(f) f
                FROM klima.brunftbergtiefe 
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2018)
    UNION
         -- Station Funtseetauern --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(cast(null as INT),0)Sonnenscheindauer,
        round(cast(null as INT),1)GlobalStrahlung,
        round(cast(null as INT),1)ReflektiertesStrahlung,
        round(cast(null as INT),1)DirektestStrahlung,
        round(tl_ftauern,1) LuftTemperatur,
        round(f_ftauern,0) RelativeLuftfeuchtigkeit,
		'Funtseetauern' Station,
		to_number('12,971166') Longitude,
		to_number('47,486732') Latitude,
		to_number('2520') Altitude
	  FROM klima.kuehroint_lwd 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    AVG(tl_ftauern) tl,
                    AVG(f_ftauern) f
                    FROM klima.kuehroint_lwd 
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2015)
   UNION
         -- Station Hinterberghorn --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(cast(null as INT),0)Sonnenscheindauer,
        round(ge,1)GlobalStrahlung,
        round(gr,1)ReflektiertesStrahlung,
        round(cast(null as INT),1)DirektestStrahlung,
        round(tl,1) LuftTemperatur,
        round(f,0) RelativeLuftfeuchtigkeit,
		'Hinterberghorn' Station,
		to_number('12,839332') Longitude,
		to_number('47,550667') Latitude,
		to_number('2493') Altitude
	  FROM klima.hinterberghorn 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    SUM(ge)ge,
                    SUM(gr)gr,
                    AVG(tl)tl,
                    AVG(f) f
                FROM klima.hinterberghorn 
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2018)
   UNION
         -- Station Hinterseeau --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(cast(null as INT),0)Sonnenscheindauer,
        round(ge,1)GlobalStrahlung,
        round(gr,1)ReflektiertesStrahlung,
        round(cast(null as INT),1)DirektestStrahlung,
        round(tl,1) LuftTemperatur,
        round(f,0) RelativeLuftfeuchtigkeit,
		'Hinterseeau' Station,
		to_number('12,826324') Longitude,
		to_number('47,586349') Latitude,
		to_number('1240') Altitude
	  FROM klima.hinterseeau 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    SUM(ge)ge,
                    SUM(gr)gr,
                    AVG(tl)tl,
                    AVG(f) f
                FROM klima.hinterseeau 
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2018)				
    UNION
         -- Station Höllgraben --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(cast(null as INT),0)Sonnenscheindauer,
        round(cast(null as INT),1)GlobalStrahlung,
        round(cast(null as INT),1)ReflektiertesStrahlung,
        round(cast(null as INT),1)DirektestStrahlung,
        round(tl,1) LuftTemperatur,
        round(f,0) RelativeLuftfeuchtigkeit,
		'Höllgraben' Station,
		to_number('13,006472') Longitude,
		to_number('47,615916') Latitude,
		to_number('660') Altitude
	  FROM klima.hoellgraben_lwd 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    AVG(tl)tl,
                    AVG(f) f
                FROM klima.hoellgraben_lwd 
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2018)
    UNION
         -- Station Jenner --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(cast(null as INT),0)Sonnenscheindauer,
        round(cast(null as INT),1)GlobalStrahlung,
        round(cast(null as INT),1)ReflektiertesStrahlung,
        round(cast(null as INT),1)DirektestStrahlung,
        round(tl,1) LuftTemperatur,
        round(f,0) RelativeLuftfeuchtigkeit,
		'Jenner' Station,
		to_number('13,017670') Longitude,
		to_number('47,585650') Latitude,
		to_number('1220') Altitude
	  FROM klima.jenner_lwd 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    AVG(tl)tl,
                    AVG(f) f
                FROM klima.jenner_lwd 
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2018)
    UNION
         -- Station Kühroint --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(cast(null as INT),0)Sonnenscheindauer,
        round(ge,1)GlobalStrahlung,
        round(gr,1)ReflektiertesStrahlung,
        round(cast(null as INT),1)DirektestStrahlung,
        round(tl_kueh,1) LuftTemperatur,
        round(f_kueh,0) RelativeLuftfeuchtigkeit,
		'Kühroint' Station,
		to_number('12,959716') Longitude,
		to_number('47,570131') Latitude,
		to_number('1420') Altitude
	  FROM klima.kuehroint_lwd 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    SUM(ge)ge,
                    SUM(gr)gr,
                    AVG(tl_kueh) tl,
                    AVG(f_kueh) f
                    FROM klima.kuehroint_lwd
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2018)				
    UNION
         -- Station Reiteralm --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(cast(null as INT),0)Sonnenscheindauer,
        round(ge,1)GlobalStrahlung,
        round(gr,1)ReflektiertesStrahlung,
        round(cast(null as INT),1)DirektestStrahlung,
        round(tl1670,1) LuftTemperatur,
        round(f1670,0) RelativeLuftfeuchtigkeit,
		'Reiteralm' Station,
		to_number('12,808285') Longitude,
		to_number('47,648622') Latitude,
		to_number('1670') Altitude
	  FROM klima.reiteralpe_lwd 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    SUM(ge)ge,
                    SUM(gr)gr,
                    AVG(tl1670) tl,
                    AVG(f1670) f
                FROM klima.reiteralpe_lwd
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2018)
     UNION
         -- Station Schlunghorn --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(cast(null as INT),0)Sonnenscheindauer,
        round(ge,1)GlobalStrahlung,
        round(gr,1)ReflektiertesStrahlung,
        round(cast(null as INT),1)DirektestStrahlung,
        round(tl,1) LuftTemperatur,
        round(f,0) RelativeLuftfeuchtigkeit,
		'Schlunghorn' Station,
		to_number('13,045034') Longitude,
		to_number('47,549256') Latitude,
		to_number('2155') Altitude
	  FROM klima.schlunghorn 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    SUM(ge)ge,
                    SUM(gr)gr,
                    AVG(tl) tl,
                    AVG(f) f
                FROM klima.schlunghorn
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2018)
     UNION
         -- Station Schoenau --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(sd,0)Sonnenscheindauer,
        round(ge,1)GlobalStrahlung,
        round(cast(null as INT),1)ReflektiertesStrahlung,
        round(de,1)DirektestStrahlung,
        round(tl,1) LuftTemperatur,
        round(f,0) RelativeLuftfeuchtigkeit,
		'Schoenau' Station,
		to_number('12,991443') Longitude,
		to_number('47,603117') Latitude,
		to_number('630') Altitude
	  FROM klima.schoenau 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    SUM(sd)sd,
                    SUM(ge)ge,
                    SUM(de)de,
                    AVG(tl) tl,
                    AVG(f) f
                FROM klima.schoenau
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2018)				
      UNION
         -- Station Steinernes Meer  --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(cast(null as INT),0)Sonnenscheindauer,
        round(ge,1)GlobalStrahlung,
        round(gr,1)ReflektiertesStrahlung,
        round(cast(null as INT),1)DirektestStrahlung,
        round(tl1893_6,1) LuftTemperatur,
		round(f1893_6,0) RelativeLuftfeuchtigkeit,
		'Steinernes Meer' Station,
		to_number('12,916774') Longitude,
		to_number('47,495469') Latitude,
		to_number('1893') Altitude
	  FROM klima.steinernes_meer 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    SUM(ge)ge,
                    SUM(gr)gr,
                    AVG(tl1893_6) tl,
                    AVG(f1893_6) f
                    FROM klima.steinernes_meer
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2018)
UNION
         -- Station Trischübel --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(cast(null as INT),0)Sonnenscheindauer,
        round(ge,1)GlobalStrahlung,
        round(gr,1)ReflektiertesStrahlung,
        round(cast(null as INT),1)DirektestStrahlung,
        round(tl,1) LuftTemperatur,
		round(f,0) RelativeLuftfeuchtigkeit,
		'Trischübel' Station,
		to_number('12,910601') Longitude,
		to_number('47,526018') Latitude,
		to_number('1764') Altitude
	  FROM klima.trischuebel 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    SUM(ge)ge,
                    SUM(gr)gr,
                    AVG(tl) tl,
                    AVG(f) f
                FROM klima.trischuebel
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2018)				
    UNION
         -- Station Watzmanngrat --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(cast(null as INT),0)Sonnenscheindauer,
        round(ge,1)GlobalStrahlung,
        round(gr,1)ReflektiertesStrahlung,
        round(cast(null as INT),1)DirektestStrahlung,
        round(tl,1) LuftTemperatur,
		round(f,0) RelativeLuftfeuchtigkeit,
		'Watzmanngrat' Station,
		to_number('12,922154') Longitude,
		to_number('47,554564') Latitude,
		to_number('2635') Altitude
	  FROM klima.watzmanngrat 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    SUM(ge)ge,
                    SUM(gr)gr,
                    AVG(tl) tl,
                    AVG(f) f
                    FROM klima.watzmanngrat
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2018)
        UNION
         -- Station Watzmannhaus --
	  SELECT TO_CHAR(datum, 'rrrr-mm-dd HH24:mi') Time,
		round(row_number() over(order by TO_CHAR(datum, 'rrrr-mm-dd HH24:mi')),0) ID,
		to_number(to_char(datum, 'rrrr')) E_YEAR,
		round(cast(null as INT),0)Sonnenscheindauer,
        round(ge,1)GlobalStrahlung,
        round(gr,1)ReflektiertesStrahlung,
        round(cast(null as INT),1)DirektestStrahlung,
        round(tl,1) LuftTemperatur,
		round(f,0) RelativeLuftfeuchtigkeit,
		'Watzmannhaus' Station,
		to_number('12,932916') Longitude,
		to_number('47,5712967') Latitude,
		to_number('1928') Altitude
	  FROM klima.watzmannhaus 
        WHERE datum IN  (
            SELECT datum FROM (
                SELECT to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24') datum,
                    SUM(ge)ge,
                    SUM(gr)gr,
                    AVG(tl) tl,
                    AVG(f) f
                FROM klima.watzmannhaus
                GROUP BY to_date(TO_CHAR(datum, 'dd.mm.rrrr hh24'), 'dd.mm.rrrr hh24'))
				WHERE EXTRACT(YEAR FROM datum) = 2018)))

                
insert into user_sdo_geom_metadata values (
'sun_conditions_hourly_mview', 'Shape', 
    MDSYS.SDO_DIM_ARRAY (
    MDSYS.SDO_DIM_ELEMENT('X', 12.5, 13.5, 0.00005),
	MDSYS.SDO_DIM_ELEMENT('Y', 47.4, 48.5, 0.00005)
  ),
  8307
  );  
create index sunpcond_hourly_mview_IDX ON klima.sun_conditions_hourly_mview (Shape) INDEXTYPE IS MDSYS.SPATIAL_INDEX;
                
                
                  
                
