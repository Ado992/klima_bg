CREATE MATERIALIZED VIEW sun_conditions_yearly_mview
      REFRESH NEXT TRUNC(SYSDATE+1/24,'HH')	AS
select time, round(avg(lufttemperatur),1)as MEANTEMP,round(sum(sonnenscheindauer),1) as sonnenscheindauer,round(avg(globalstrahlung),1)AS globalstrahlung,round(avg(reflektiertesstrahlung),1) as reflektiertesstrahlung, round(avg(direktesstrahlung),1) as direktesstrahlung from (
SELECT TIME,LUFTTEMPERATUR,sonnenscheindauer,globalstrahlung,reflektiertesstrahlung,direktesstrahlung
 from (  SELECT * FROM (
    -- Station Blaueis --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl,1) Lufttemperatur,
        round(null,0) Sonnenscheindauer,
        round(ge,1)Globalstrahlung,
        round(null,1)Reflektiertesstrahlung,
        round(null,1)Direktesstrahlung
		FROM klima.blaueis
        
        
 UNION
         -- Station Brunftbergtiefe --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl,1) Lufttemperatur,
        round(null,0) Sonnenscheindauer,
        round(ge,1)Globalstrahlung,
        round(gr,1)Reflektiertesstrahlung,
        round(null,1)Direktesstrahlung
		FROM klima.brunftbergtiefe
        
UNION
         -- Station Funtseetauern --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl_ftauern,1) LuftTemperatur,
        round(null,0) Sonnenscheindauer,
        round(null,1)Globalstrahlung,
        round(null,1)Reflektiertesstrahlung,
        round(null,1)Direktesstrahlung
		FROM klima.kuehroint_lwd 
        
UNION
         -- Station Hinterberghorn --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl,1) LuftTemperatur,
        round(null,0) Sonnenscheindauer,
        round(ge,1)Globalstrahlung,
        round(gr,1)Reflektiertesstrahlung,
        round(null,1)Direktesstrahlung
		FROM klima.hinterberghorn 
        
UNION
         -- Station Hinterseeau --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl,1) LuftTemperatur,
        round(null,0) Sonnenscheindauer,
        round(ge,1)Globalstrahlung,
        round(gr,1)Reflektiertesstrahlung,
        round(null,1)Direktesstrahlung
		FROM klima.hinterseeau 
                   
UNION
         -- Station Höllgraben --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl,1) LuftTemperatur,
        round(null,0) Sonnenscheindauer,
        round(null,1)Globalstrahlung,
        round(null,1)Reflektiertesstrahlung,
        round(null,1)Direktesstrahlung
		FROM klima.hoellgraben_lwd 
      
UNION
         -- Station Jenner --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl,1) LuftTemperatur,
        round(null,0) Sonnenscheindauer,
        round(null,1)Globalstrahlung,
        round(null,1)Reflektiertesstrahlung,
        round(null,1)Direktesstrahlung
		FROM klima.jenner_lwd 
       
UNION
         -- Station Kühroint --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl_kueh,1) LuftTemperatur,
        round(null,0) Sonnenscheindauer,
        round(ge,1)Globalstrahlung,
        round(gr,1)Reflektiertesstrahlung,
        round(null,1)Direktesstrahlung
		FROM klima.kuehroint_lwd 
                  
UNION
         -- Station Reiteralm --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl1670,1) LuftTemperatur,
        round(null,0) Sonnenscheindauer,
        round(ge,1)Globalstrahlung,
        round(gr,1)Reflektiertesstrahlung,
        round(null,1)Direktesstrahlung
		FROM klima.reiteralpe_lwd 
        
UNION
         -- Station Schlunghorn --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl,1) LuftTemperatur,
        round(null,0) Sonnenscheindauer,
        round(ge,1)Globalstrahlung,
        round(gr,1)Reflektiertesstrahlung,
        round(null,1)Direktesstrahlung
		FROM klima.schlunghorn 
       
UNION
         -- Station Schoenau --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl,1) LuftTemperatur,
        round(sd,0) Sonnenscheindauer,
        round(ge,1)Globalstrahlung,
        round(null,1)Reflektiertesstrahlung,
        round(de,1)Direktesstrahlung
		 FROM klima.schoenau 
                
UNION
         -- Station Steinernes Meer  --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl1893_6,1) LuftTemperatur,
        round(null,0) Sonnenscheindauer,
        round(ge,1)Globalstrahlung,
        round(gr,1)Reflektiertesstrahlung,
        round(null,1)Direktesstrahlung
		FROM klima.steinernes_meer 
        
UNION
         -- Station Trischübel --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl,1) LuftTemperatur,
        round(null,0) Sonnenscheindauer,
        round(ge,1)Globalstrahlung,
        round(gr,1)Reflektiertesstrahlung,
        round(null,1)Direktesstrahlung
		FROM klima.trischuebel 
         
UNION
         -- Station Watzmanngrat --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl,1) LuftTemperatur,
        round(null,0) Sonnenscheindauer,
        round(ge,1)Globalstrahlung,
        round(gr,1)Reflektiertesstrahlung,
        round(null,1)Direktesstrahlung
		FROM klima.watzmanngrat 
       
UNION
         -- Station Watzmannhaus --
	  SELECT TO_CHAR(datum, 'rrrr') Time,
		round(tl,1) LuftTemperatur,
        round(null,0) Sonnenscheindauer,
        round(ge,1)Globalstrahlung,
        round(gr,1)Reflektiertesstrahlung,
        round(null,1)Direktesstrahlung
		 FROM klima.watzmannhaus 
        )))    
group by time

