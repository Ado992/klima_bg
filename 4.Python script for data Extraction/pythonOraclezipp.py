#Importing modules
import os
import cx_Oracle
import csv
import zipfile
# Connect with DB with connect method and SQL variable
connection = cx_Oracle.connect('klima/klima0815@ispacevm19:1521/orageo')
SQL = "SELECT * FROM AIR_CONDITIONS_YEARLY_MVIEW ORDER BY TIME"
#Output variable
filename="sample6.csv"
#Cursor creation and connection, opening the file and using writer functions for writing sql to our output csv
#Using writer objects to write row parameters(field names) to our csv and zipfile methods to zip our csv
cursor = connection.cursor()
cursor.execute(SQL)
with open (filename, 'w') as output:
    writer = csv.writer (output)
    writer.writerow([i[0]for i in cursor.description])
    writer.writerows(cursor.fetchall())
cursor.close()
connection.close()
air_zip = zipfile.ZipFile("sample6.zip", 'w')
air_zip.write(filename, compress_type = zipfile.ZIP_DEFLATED)
air_zip.close()
os.unlink(filename) #lefting out csv file while storing only zipped folder