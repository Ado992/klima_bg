# KLIMA_BG

Opening up climate data of National Park Berchtesgaden


*  WMS: https://ispacevm55.researchstudio.at:6443/arcgis/services/npbg/klima_bg/MapServer/WMSServer?request=GetCapabilities&service=WMS

*  WFS: https://ispacevm55.researchstudio.at:6443/arcgis/services/npbg/klima_bg_wfs/MapServer/WFSServer?request=GetCapabilities&service=WFS
*  OpsDashboard: http://agiadnansabic.maps.arcgis.com/apps/opsdashboard/index.html#/cd4b70d05ca64cbe80e666dfcf486b21
*  APEX:https://ispacevm08.researchstudio.at/npbg/f?p=102:1:17087612396787:::::